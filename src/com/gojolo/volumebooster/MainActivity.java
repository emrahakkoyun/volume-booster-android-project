package com.gojolo.volumebooster;


import android.app.Activity;
import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.widget.SeekBar;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class MainActivity extends Activity {
	  SeekBar alarm=null;
	  SeekBar music=null;
	  SeekBar ring=null;
	  SeekBar system=null;
	  SeekBar voice=null;
	  AudioManager mgr=null;
		private InterstitialAd interstitial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// Prepare the Interstitial Ad
		AdRequest adRequest = new AdRequest.Builder().build(); 
				interstitial = new InterstitialAd(MainActivity.this);
				// Insert the Ad Unit ID
				interstitial.setAdUnitId("ca-app-pub-1312048647642571/2105753446");
				interstitial.loadAd(adRequest);
				interstitial.setAdListener(new AdListener() {
					public void onAdLoaded() {
						// Call displayInterstitial() function
						displayInterstitial();
					}
				});
		SeekBar music =(SeekBar)findViewById(R.id.Seek1);
		SeekBar alarm =(SeekBar)findViewById(R.id.Seek2);
		SeekBar voice =(SeekBar)findViewById(R.id.Seek3);
		SeekBar ring =(SeekBar)findViewById(R.id.Seek4);
		SeekBar notification =(SeekBar)findViewById(R.id.Seek5);
		SeekBar system =(SeekBar)findViewById(R.id.Seek6);
		SeekBar seek7 =(SeekBar)findViewById(R.id.Seek7);
		   initBar(alarm, AudioManager.STREAM_ALARM);
		    initBar(music, AudioManager.STREAM_MUSIC);//for Volume this is neccessary
		    initBar(ring, AudioManager.STREAM_RING);
		    initBar(system, AudioManager.STREAM_SYSTEM);
		    initBar(voice, AudioManager.STREAM_VOICE_CALL);
		    initBar(notification,AudioManager.STREAM_NOTIFICATION);
		    initBar(notification,AudioManager.STREAM_DTMF);
	}
	 private void initBar(SeekBar bar, final int stream) {
		 try{
	            mgr=  (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	            final AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_VOICE_CALL,
	                    11150, AudioFormat.CHANNEL_OUT_MONO,
	                    AudioFormat.ENCODING_PCM_16BIT, 15522,
	                    AudioTrack.MODE_STATIC);
			 bar.setMax(mgr
	                    .getStreamMaxVolume(stream));
		    bar.setProgress(mgr.getStreamVolume(stream));
		    bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
		      public void onProgressChanged(SeekBar bar, int progress,
		                                    boolean fromUser) {
		       mgr.setStreamVolume(stream, progress,
		    		   AudioManager.FLAG_SHOW_UI);
		 //      mgr.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);

		      }

		      public void onStartTrackingTouch(SeekBar bar) {
		        // no-op
		      }

		      public void onStopTrackingTouch(SeekBar bar) {
		        // no-op
		      }
		    });
		 }
			catch(Exception e){
				e.printStackTrace();
			}
		  }
	public void displayInterstitial() {
		// If Ads are loaded, show Interstitial else show nothing.
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}
}
